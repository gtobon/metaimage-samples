# README #

The purpose of this repository is to host testing data of the MetaImage format.  The data are used for testing [nibabel](https://github.com/nipy/nibabel).

### For questions, contact: ###

Repo owner: gtobon